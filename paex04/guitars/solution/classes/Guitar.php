<?php
class Guitar {
	public $title;
	public $price;
	public $picture;
	public $ytlink;
		
	public static function parse($row){
		$guitarNameAndDetails = explode("|",$row);
		$res 		= new Guitar();
		$res->title 	= $guitarNameAndDetails[0];
		$details 	= explode(" ",$guitarNameAndDetails[1]);
		$res->picture 	= $details[0];
		$res->price	= $details[1];
		$res->ytlink	= $details[2]; 
		return $res;
	}
	
	public static function getGuitars($filename){
		$guitarsArray = file($filename);
		$res = [];
		foreach($guitarsArray as $guitarRow){
			$res[] = Guitar::parse($guitarRow);
		}
		return $res;
	}
}