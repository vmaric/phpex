<?php
session_start();
require "model.php";   
$cardExist = isset($_SESSION['card']); 
if(isset($_GET['remove'])){
	$id = $_GET['remove'];
	if($cardExist){
		if(isset($_SESSION['card'][$id])){
			if($_SESSION['card'][$id]<=1){
				unset($_SESSION['card'][$id]);
			} else {
				$_SESSION['card'][$id]-=1;
			}
		}
	}
}
if(isset($_GET['removeall'])){
	$id = $_GET['removeall'];
	if($cardExist){
		if(isset($_SESSION['card'][$id])){
			unset($_SESSION['card'][$id]);
		}
	}
}
if(isset($_GET['clear'])){ 
	if($cardExist){
		$_SESSION['card'] = [];
	}
}
if($cardExist){
	$totalPrice = 0;
	foreach($_SESSION['card'] as $k=>$v){
		$item = $products[$k];
		$totalPrice += ($v*$item['price']);
		echo "<h3>{$item['name']} ({$v})</h3>";
		echo "<img width='100' src='{$item['picture']}' /><br>";
		echo "<a href='?remove={$k}'>Remove item</a> / ";
		echo "<a href='?removeall={$k}'>Remove All items</a>";
	}
	echo "<br><a href='?clear'>Clear basket</a>";
	echo "<hr>";
	echo "Total price: {$totalPrice}";
} else {
	echo "Your card is empty or does not exist";
}
echo "<br><a href='index.php'>Back to main page</a>";

